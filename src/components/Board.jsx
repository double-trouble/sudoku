import React from "react";
// import PropTypes from "prop-types";

import Block from "./Block";
import * as styles from "../styles/Board.module.css";

const Board = (props) => {
  return (
    <div className={styles.container}>
      <div className={styles.board}>
        <Block /> <Block /> <Block /> <Block /> <Block /> <Block /> <Block />
        <Block /> <Block />
      </div>
    </div>
  );
};

Board.propTypes = {};

export default Board;
