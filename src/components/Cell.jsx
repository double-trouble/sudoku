import { Fragment } from "react";
import PropTypes from "prop-types";

import * as styles from "../styles/Cell.module.css";

const Cell = ({ name, value, handleChange }) => {
  return (
    <Fragment>
      <div className={styles.container}>
        <input
          className={styles.input}
          type="text"
          name={name}
          size="2"
          maxLength="1"
          value={value}
          onChange={handleChange}
        />
      </div>
    </Fragment>
  );
};

Cell.propTypes = {
  name: PropTypes.string,
  value: PropTypes.string,
  handleChange: PropTypes.func,
};

export default Cell;
