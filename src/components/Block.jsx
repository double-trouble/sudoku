// import PropTypes from "prop-types";

import Cell from "./Cell";
import * as styles from "../styles/Block.module.css";

const Block = (props) => {
  return (
    <div className={styles.container}>
      <Cell />
      <Cell />
      <Cell />
      <Cell />
      <Cell />
      <Cell />
      <Cell />
      <Cell />
      <Cell />
    </div>
  );
};

Block.propTypes = {};

export default Block;
