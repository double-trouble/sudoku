import Board from "./components/Board";

import * as styles from "./styles/App.module.css";
function App() {
  return (
    <div className={styles.container}>
      <Board />
    </div>
  );
}

export default App;
