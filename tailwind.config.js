const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false,
  theme: {
    extend: {
      colors: {
        primary: "#0B132B",
        secondary: "#6FFFE9",
        "dark-001": "#0B132B",
        teal: {
          100: "#ccf3eb",
          200: "#9ae7d7",
          300: "#67dbc2",
          400: "#35cfae",
          500: "#02c39a",
          600: "#029c7b",
          700: "#01755c",
          800: "#014e3e",
          900: "#00271f",
        },
      },
      fontFamily: {
        sans: ["Comfortaa", "Raleway", ...defaultTheme.fontFamily.sans],
        serif: ["Quicksand", ...defaultTheme.fontFamily.serif],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: ["@tailwindcss/forms", "@tailwindcss/line-clamp"],
};
